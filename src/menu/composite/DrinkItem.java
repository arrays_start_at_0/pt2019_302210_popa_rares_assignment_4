package menu.composite;

public class DrinkItem extends BaseItem{
    private double volume;

    public DrinkItem(String name, double price, boolean isPairable, double volume) {
        super("DRINK", name, price, isPairable);
        this.volume = volume;
    }

    public DrinkItem(String name, double price, double weight) {//default is true
        super("DRINK", name, price, true);
        this.volume = weight;
    }
    double getVolume() { return volume; }

    @Override
    public String toString() {
        return  super.toString() +
                "\n\tVolume: " + getVolume() + "\n";
    }
}

