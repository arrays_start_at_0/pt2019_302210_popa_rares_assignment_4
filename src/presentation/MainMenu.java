package presentation;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainMenu extends Application {
    private final int WIDTH = 400;
    private final int HEIGHT = 400;

    private Controller C;

    MainMenu(Controller controller) {
        C = controller;
    }

    public void start(Stage menuStage) {
        Button orderButton = new Button("Make an order");
        orderButton.setFont(new Font(30));
        orderButton.setPrefHeight(Integer.MAX_VALUE);
        orderButton.setPrefWidth(Integer.MAX_VALUE);
        orderButton.setOnAction(e -> C.launchOrder());

        Button chefButton = new Button("Chef Operations");
        chefButton.setFont(new Font(30));
        chefButton.setPrefHeight(Integer.MAX_VALUE);
        chefButton.setPrefWidth(Integer.MAX_VALUE);
        chefButton.setOnAction(e -> C.launchChef());

        Button editMenu = new Button("Edit menu");
        editMenu.setFont(new Font(30));
        editMenu.setPrefHeight(Integer.MAX_VALUE);
        editMenu.setPrefWidth(Integer.MAX_VALUE);
        editMenu.setOnAction(e -> C.launchEdit());

        VBox mainLayout = new VBox(20);
        mainLayout.setPadding(new Insets(10));
        mainLayout.getChildren().addAll(orderButton, chefButton, editMenu);
        Scene mainMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        menuStage.setScene(mainMenu);
        menuStage.setTitle("Main Menu");
        menuStage.show();
    }
}
