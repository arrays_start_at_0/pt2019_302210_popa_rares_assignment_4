package file_writer;

import menu.MenuItemWrapper;
import model.Order;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Receipt {

    public static void writeReceipt(Order O) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        String fileName = "Order no" + O.getID() + " - " + date + ".txt";
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(("Date: " + date).getBytes());
            fos.write("\n\nOrder contents: ".getBytes());
            for(MenuItemWrapper I : O.getContentList()) {
                fos.write(( "\n\t" + I.getName() + (I.getAmmount() > 1 ? " x" + I.getAmmount() : "") +
                            (I.getType().equals("FOOD") ? "\n\tWeight: " : "\n\tVolume: ") + I.getQuantity() +
                            "\n\tPrice: " + (I.getPrice() * I.getAmmount()) + "\n").getBytes());
            }
            fos.write(("\n\nTotal: " + O.getTotal()).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
